<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230126165124 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Courier enttiy created';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE courier (id INT AUTO_INCREMENT NOT NULL, transport_id INT NOT NULL, user_id INT NOT NULL, given_name VARCHAR(255) NOT NULL, family_name VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, rating INT NOT NULL, status TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, is_deleted TINYINT(1) NOT NULL, INDEX IDX_CF134C7C9909C13F (transport_id), INDEX IDX_CF134C7CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE courier ADD CONSTRAINT FK_CF134C7C9909C13F FOREIGN KEY (transport_id) REFERENCES transport (id)');
        $this->addSql('ALTER TABLE courier ADD CONSTRAINT FK_CF134C7CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE media_object ADD courier_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE media_object ADD CONSTRAINT FK_14D43132E3D8151C FOREIGN KEY (courier_id) REFERENCES courier (id)');
        $this->addSql('CREATE INDEX IDX_14D43132E3D8151C ON media_object (courier_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE media_object DROP FOREIGN KEY FK_14D43132E3D8151C');
        $this->addSql('ALTER TABLE courier DROP FOREIGN KEY FK_CF134C7C9909C13F');
        $this->addSql('ALTER TABLE courier DROP FOREIGN KEY FK_CF134C7CA76ED395');
        $this->addSql('DROP TABLE courier');
        $this->addSql('DROP INDEX IDX_14D43132E3D8151C ON media_object');
        $this->addSql('ALTER TABLE media_object DROP courier_id');
    }
}
