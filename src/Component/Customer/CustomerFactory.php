<?php

namespace App\Component\Customer;

use App\Entity\Customer;
use App\Entity\User;
use DateTime;

class CustomerFactory
{
    public function create(User $user, string $givenName, string $familyName, string $phone): Customer
    {
      return (new Customer())
            ->setUser($user)
            ->setGivenName($givenName)
            ->setFamilyName($familyName)
            ->setPhone($phone)
            ->setCreatedAt(new DateTime());
    }
}