<?php

namespace App\Component\Courier;

use App\Entity\Courier;
use App\Entity\MediaObject;
use App\Entity\Transport;
use App\Entity\User;
use DateTime;

class CourierFactory
{
    public function create(
        User $user,
        Transport $transport,
        MediaObject $mediaObject,
        string $givenName,
        string $familyName,
        string $phone,
    ): Courier {

        return (new Courier())
            ->setUser($user)
            ->setTransport($transport)
            ->setImage($mediaObject)
            ->setGivenName($givenName)
            ->setFamilyName($familyName)
            ->setPhone($phone)
            ->setCreatedAt(new DateTime());
    }
}
