<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\DeleteAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Entity\Interfaces\UserSettableInterface;
use App\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
#[ApiResource
(
    collectionOperations: [
        'get' => [
            'security' => "is_granted('ROLE_USER')",
            'normalization_context' => ['groups' => ['orders:read']],
        ],
        'post'=>[
            'denormalization_context' => ['groups' => ['orders:read','orders:write']],

        ],
    ],
    itemOperations: [
        'get' => [],
        'put' => [
            'security' => "object.getUser() == user || is_granted('ROLE_ADMIN')",
            'denormalization_context' => ['groups' => ['order:put:write']],
        ],
        'delete' => [
            'controller' => DeleteAction::class,
            'security' => "object.getUser() == user || is_granted('ROLE_ADMIN')",
        ],
    ],
    denormalizationContext: ['groups' => ['order:write']],
    normalizationContext: ['groups' => ['order:read', 'orders:read']],
)]
#[ApiFilter(OrderFilter::class, properties: ['id', 'createdAt', 'updatedAt'])]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'title' => 'partial'])
]

class Order implements
    UserSettableInterface,
    CreatedAtSettableInterface,
    UpdatedAtSettableInterface,
    IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['orders:read'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['orders:read', 'order:put:write'])]
    private $title;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['orders:read', 'order:put:write'])]
    private $cargoType;

    #[ORM\Column(type: 'integer')]
    #[Groups(['orders:read', 'order:put:write'])]
    private $weight;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['orders:read', 'order:put:write'])]
    private $size;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['orders:read', 'order:put:write'])]
    private $whereFrom;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['orders:read', 'order:put:write'])]
    private $whereTo;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['orders:read', 'order:put:write'])]
    private $untilWhen;

    #[ORM\Column(type: 'integer')]
    #[Groups(['orders:read'])]
    private $status;

    #[ORM\Column(type: 'datetime')]
    private $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $updatedAt;

    #[ORM\Column(type: 'boolean')]
    private $isDeleted = false;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['orders:read'])]
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCargoType(): ?string
    {
        return $this->cargoType;
    }

    public function setCargoType(string $cargoType): self
    {
        $this->cargoType = $cargoType;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getWhereFrom(): ?string
    {
        return $this->whereFrom;
    }

    public function setWhereFrom(string $whereFrom): self
    {
        $this->whereFrom = $whereFrom;

        return $this;
    }

    public function getWhereTo(): ?string
    {
        return $this->whereTo;
    }

    public function setWhereTo(string $whereTo): self
    {
        $this->whereTo = $whereTo;

        return $this;
    }

    public function getUntilWhen(): ?string
    {
        return $this->untilWhen;
    }

    public function setUntilWhen(?string $untilWhen): self
    {
        $this->untilWhen = $untilWhen;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): self
    {
        $this->user = $user;

        return $this;
    }
}
