<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Component\Courier\Dtos\SignUpCourierRequestDto;
use App\Controller\Courier\SignUpCourierAction;
use App\Controller\DeleteAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Repository\CourierRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CourierRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get' => [
            'security' => "is_granted('ROLE_USER')",
            'normalization_context' => ['groups' => ['courier:read']],
        ],
        'SignUp' => [
            'method' => 'post',
            'path' => '/couriers',
            'controller' => SignUpCourierAction::class,
            'input' => SignUpCourierRequestDto::class
        ]
    ],
    itemOperations: [
        'get'            => [],
        'put' => [
            'security' => "object.getUser() == user || is_granted('ROLE_ADMIN')",
            'denormalization_context' => ['groups' => ['courier:put:write']],
        ],
        'delete' => [
            'controller' => DeleteAction::class,
            'security' => "object.getUser() == user || is_granted('ROLE_ADMIN')",
        ],
    ],
    denormalizationContext: ['groups' => ['courier:write']],
    normalizationContext: ['groups' => ['courier:read', 'couriers:read']],
)]
#[ApiFilter(OrderFilter::class, properties: ['id', 'createdAt', 'updatedAt', 'email'])]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'email' => 'partial'])]
class Courier implements
    UpdatedAtSettableInterface,
    IsDeletedSettableInterface,
    CreatedAtSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['courier:read', 'courier:put:write'])]
    private $familyName;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['courier:read', 'courier:put:write'])]
    private $givenName;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['courier:read', 'courier:put:write'])]
    private $phone;

    #[ORM\Column(type: 'integer')]
    private $rating = 0;

    #[ORM\Column(type: 'boolean')]
    private $status = false;

    #[ORM\Column(type: 'datetime')]
    private $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $updatedAt;

    #[ORM\Column(type: 'boolean')]
    private $isDeleted = false;

    #[ORM\ManyToOne(targetEntity: MediaObject::class)]
    #[Groups(['courier:read', 'courier:put:write',])]
    private $image;

    #[ORM\ManyToOne(targetEntity: Transport::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['courier:read'])]
    private $transport;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'couriers')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['courier:read'])]
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFamilyName(): ?string
    {
        return $this->familyName;
    }

    public function setFamilyName(string $familyName): self
    {
        $this->familyName = $familyName;

        return $this;
    }

    public function getGivenName(): ?string
    {
        return $this->givenName;
    }

    public function setGivenName(string $givenName): self
    {
        $this->givenName = $givenName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function isStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getImage(): ?MediaObject
    {
        return $this->image;
    }

    public function setImage(?MediaObject $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getTransport(): ?Transport
    {
        return $this->transport;
    }

    public function setTransport(?Transport $transport): self
    {
        $this->transport = $transport;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
