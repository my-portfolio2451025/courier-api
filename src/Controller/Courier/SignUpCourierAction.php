<?php

namespace App\Controller\Courier;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Component\Courier\CourierFactory;
use App\Component\Courier\Dtos\SignUpCourierRequestDto;
use App\Component\Transport\TransportFactory;
use App\Component\User\UserFactory;
use App\Component\User\UserManager;
use App\Entity\Courier;
use App\Repository\CourierRepository;
use App\Repository\TransportRepository;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class SignUpCourierAction extends AbstractController
{
    public function __invoke(
        UserFactory $userFactory,
        UserManager $userManager,
        TransportFactory $transportFactory,
        TransportRepository $transportRepository,
        CourierFactory $courierFactory,
        CourierRepository $courierRepository,
        Request $request,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
    ): Courier {
        /** @var SignUpCourierRequestDto $signUpCourier */
        $signUpCourier = $serializer->deserialize($request->getContent(), SignUpCourierRequestDto::class, 'json');
        $user = $userFactory->create($signUpCourier->getEmail(), $signUpCourier->getPassword());

        $validator->validate($user);

        $transport = $transportFactory->create(
            $signUpCourier->getType(),
            $signUpCourier->getSerialNumber(),
            $signUpCourier->getStateNumber(),
            $signUpCourier->getCarImage()
        );

            $transportRepository->add($transport);

        $courier = $courierFactory->create(
            $user,
            $transport,
            $signUpCourier->getImage(),
            $signUpCourier->getGivenName(),
            $signUpCourier->getFamilyName(),
            $signUpCourier->getPhone(),
        );

        $validator->validate($signUpCourier);

        $userManager->save($user);
        $courierRepository->add($courier, true);

        return $courier;
    }
}
